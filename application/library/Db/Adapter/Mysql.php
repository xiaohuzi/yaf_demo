<?php
/**
 * mysql连接操作类
 * @author xiaohuzi
 */
class Db_Adapter_Mysql extends Db_Adapter
{

    /**
     * PDO type.
     *
     * @var string
     */
    protected $_pdo_type = 'mysql';
    protected $_db_master;
    protected $_db_slave;
        
    /**
     * 支持的详细的数字类型
     *
     * @var array
     */
    protected $_numeric_data_types = array(
        Db::TYPE_INT => Db::TYPE_INT,
        Db::TYPE_BIGINT => Db::TYPE_BIGINT,
        Db::TYPE_FLOAT => Db::TYPE_FLOAT,
        'INT' => Db::TYPE_INT,
        'INTEGER' => Db::TYPE_INT,
        'MEDIUMINT' => Db::TYPE_INT,
        'SMALLINT' => Db::TYPE_INT,
        'TINYINT' => Db::TYPE_INT,
        'BIGINT' => Db::TYPE_BIGINT,
        'SERIAL' => Db::TYPE_BIGINT,
        'DEC' => Db::TYPE_FLOAT,
        'DECIMAL' => Db::TYPE_FLOAT,
        'DOUBLE' => Db::TYPE_FLOAT,
        'DOUBLE PRECISION' => Db::TYPE_FLOAT,
        'FIXED' => Db::TYPE_FLOAT,
        'FLOAT' => Db::TYPE_FLOAT
    );
    
    /**
     * Creates a PDO object and connects to the database.
     *
     * @return void
     * @throws DbException
     */
    protected function _connect($master=0)
    {
        // 实现每种数据库的单例
        if(empty($this->_db_master)){
            $this->_db_master=$this->_config['host_m'];
            $this->_db_slave=$this->_config['host_s'];
        }
        
        if($master==1){
            $this->_config['host'] = $this->_db_master;
            $key = $this->_config['host'].'_'.$this->_config['port'].'_'.$this->_config['dbname'];
        }else{
            $this->_config['host'] = $this->_db_slave;
            $key = $this->_config['host'].'_'.$this->_config['port'].'_'.$this->_config['dbname'];
        }

      if (isset (self::$_connection_arr [$key]) ) {
          $this->_connection = self::$_connection_arr [$key];
          return;
      }else{
          parent::_connect();
          self::$_connection_arr [$key] = $this->_connection;
      }

        // 设置存取字符集
      $this->exec('SET NAMES utf8', $master);
    }

    /**
     * 获取用来 quote 标识符的字符
     *
     * @return string
     */
    public function getQuoteIdentifierSymbol()
    {
        return "`";
    }

    /**
     * 获取当前数据库的数据表列表
     *
     * @return array
     */
    public function listTables()
    {
        return $this->fetchCol('SHOW TABLES');
    }

    /**
     * 处理 limit offset 的 sql 语句
     *
     * @param string $sql 需要被加上 limit offset 处理的 sql 语句
     * @param integer $count 需要获取的记录数
     * @param integer $offset 记录起始偏移量, 从 0 开始.
     * @return string 用于处理此功能的 SQL
     */
    public function limit($sql, $count, $offset = 0)
    {
        $count = intval($count);
        if ($count <= 0) {
            /**
             * @see DbException
             */

            throw new Db_Exception ("LIMIT argument count=$count is not valid");
        }

        $offset = intval($offset);
        if ($offset < 0) {
            /**
             * @see DbException
             */
            throw new Db_Exception ("LIMIT argument offset=$offset is not valid");
        }

        $sql .= " LIMIT $count";
        if ($offset > 0) {
            $sql .= " OFFSET $offset";
        }

        return $sql;
    }
}