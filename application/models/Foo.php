<?php
/**
 * @name FooModel
 * @author xiaohuzi
 * @desc Model类
 * @see http://www.php.net/manual/en/class.yaf-controller-abstract.php
 */
class FooModel extends BaseModel
{
    protected $table = 'users';

    public function foo()
    {
        echo 'this is model foo';
    }

}