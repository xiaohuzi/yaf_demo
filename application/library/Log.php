<?php
use \Monolog\Logger;
use \Monolog\Handler\StreamHandler;

/**
 * Created by PhpStorm.
 * User: xiaohuzi
 * Date: 2017/9/29
 * Time: 上午10:38
 */

class Log
{

    protected static $_file_name = 'yaf';
    protected static $_log_path = '';
    protected static $_handler = [];

    public static  $structure = [
        'msg' => '',
        'context' => '',
    ];


    /**
     * info
     * @param $msg
     * @param string $file_name 日志文件名称
     */
    public static function info($msg, $context = [], $file_name = '')
    {
        self::logger('info', $msg, $context, $file_name);
    }


    /**
     * debug
     * @param $msg
     * @param string $file_name 日志文件名称
     */
    public static function debug($msg, $context = [], $file_name = '')
    {
        self::logger('debug', $msg, $context, $file_name);
    }


    /**
     * warning
     * @param $msg
     * @param string $file_name 日志文件名称
     */
    public static function warning($msg, $context = [], $file_name = '')
    {
        self::logger('warning', $msg, $context, $file_name);
    }


    /**
     * error
     * @param $msg
     * @param string $file_name 日志文件名称
     */
    public static function error($msg, $context = [], $file_name = '')
    {
        self::logger('error', $msg, $context, $file_name);
    }

    protected static function logger($type, $msg, $context, $file_name)
    {
        $logger = self::getInstance($file_name);

        if (empty($context) && !is_string($msg)) {
            $context = $msg;
            $msg = '';
        }

        switch ($type) {
            case 'info':
                $logger->addInfo($msg, $context);
                break;
            case 'warning':
                $logger->addWarning($msg, $context);
                break;
            case 'error':
                $logger->addError($msg, $context);
                break;
            case 'debug':
                $logger->addDebug($msg, $context);
                break;
        }
    }


    /**
     * 简单格式化
     * @param $msg
     */
    protected static function format($msg)
    {
        if (is_array($msg)) {
            self::$structure['msg'] = '';
            $context = $msg;
        } else {
            ob_start();
            var_dump($msg);
            self::$structure['msg'] = ob_get_contents();
            ob_end_clean();
            $context = [];
        }

    }


    /**
     * 获取logger实例
     * @param $file_name
     * @return mixed
     */
    public static function getInstance($file_name)
    {
        $config = Yaf\Registry::get('config');

        if (empty(self::$_handler)) {
            self::$_log_path =  realpath(Yaf\Application::app()->getAppDirectory() . '/../') .  '/logs';

            if (isset($config->log->type)) {
                $log_type = $config->log->type;

                if ($log_type == 'daily') {
                   self::$_log_path = self::$_log_path . '/' .  date('Y-m-d');

                    if (!is_dir(self::$_log_path)) {
                        @mkdir(self::$_log_path);
                    }
                }
            }
        }

        if (isset($config->log->file) && empty($file_name)) {
            $file_name = $config->log->file;
        } else if (empty($file_name)) {
            $file_name = self::$_file_name;
        }

        if (!isset(self::$_handler[$file_name])) {
            $logger = new Logger($file_name);
            $file = self::$_log_path . '/' . $file_name . '.log';
            $processor = new Log_IntrospectionProcessor(Logger::DEBUG, ['Illuminate\\']);
            $logger->pushProcessor($processor);
            $logger->pushHandler($handler = new StreamHandler($file));

            $handler->setFormatter(new \Monolog\Formatter\LineFormatter(null, null, true, true));
            self::$_handler[$file_name] = $logger;
        }

        return self::$_handler[$file_name];
    }

}