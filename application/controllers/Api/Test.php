<?php

// use Illuminate\Database\Eloquent\Model;
// use 

use \Curl\Curl;
use \Monolog\Logger;
use \Monolog\Handler\StreamHandler;


/**
 * @name IndexController
 * @author xiaohuzi
 * @desc 默认控制器
 * @see http://php.net/manual/zh/class.yaf-controller-abstract.php
 */
class Api_TestController extends Yaf\Controller_Abstract
{
    
    public function indexAction()
    {
        echo 'Welcome Yet Another Framework';
    }

}
