<?php

error_reporting(E_ALL);
define('APPLICATION_PATH', dirname(dirname(__FILE__)));

date_default_timezone_set("Asia/Chongqing");


$application = new Yaf\Application(APPLICATION_PATH."/conf/application.ini", 'local');

$application->bootstrap()->run();
?>