<?php
/**
 * @name IndexController
 * @author xiaohuzi
 * @desc 默认控制器
 * @see http://php.net/manual/zh/class.yaf-controller-abstract.php
 */
class IndexController extends Yaf\Controller_Abstract
{

    /**
     * map 路由模式只有indexAction可以访问
     */
    public function indexAction()
    {
        echo 'Welcome Yet Another Framework';
    }

    public function viewAction()
    {
        $this->getView()->assign("name", 'php');
        $this->getView()->assign("type", 'web');
        $this->display("view");
    }

    public function modelAction()
    {
       // $user = UserModel::find(1);
        $model = new UserModel();

    $user = $model->find(1);
//        $users = UserModel::all();

//        var_dump($user->toArray());
    }

    public function curlAction()
    {
        $curl = new Curl();

        $curl->get('http://www.baidu.com');
        var_dump($curl->response);
    }

    public function ExceptionAction()
    {
        throw new Exception("Error Processing Request", 1);

    }

}
