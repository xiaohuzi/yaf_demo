Yaf 框架代码
================================================

Overview
--------

The Yet Another Framework (Yaf) 扩展是一个用来开发web应用的php框架。

快速入门请参见 教程 .
[http://www.laruence.com/manual/index.html]()

### Requirements
* composer
* yaf extension

## Getting started

### Installation
1. Clone the source code

```
https://pecl.php.net/package/yaf
php5.6 推荐 V2.3.5 
php7 推荐 V3.0.5
```
1. Go to the source code directory

```
cd /path/to/yaf-src/
phpize
./configure
make
sudo make install
	```
1. Build and install the PHP extension (follow instructions at [php.net][2])

	```
	[yaf]
	extension="/yourself_path/yaf.so"
	yaf.use_namespace=1
	```

	1. Install plugin dependencies

	```
	composer install
	```

### Usage

	1. IndexController

	```
	访问地址
	http://yaf.local/

	```
	1. model 测试

	```
	访问地址
	http://yaf.local/api/model

	```
	1. Http 请求 （包装\Curl\Curl）

	```
	访问地址
	http://yaf.local/http

	```
1. Log 日志 (包装Monolog)

	```
	访问地址
	http://yaf.local/log

	```
