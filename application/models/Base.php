<?php

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use Yaf\Registry as YRegistry;


class BaseModel extends Model
{
    protected $_connection_name = 'default';

    protected $_attributes = [
        'driver'    => 'mysql',
        'host'      => 'localhost',
        'database'  => 'test',
        'username'  => 'root',
        'password'  => '',
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
    ];

    protected static $_connection = [];

    public function __construct(array $params = [])
    {
        parent::__construct($params);
        $this->initConnection();
    }


    /**
     * 初始数据库连接信息
     * @return bool
     * @throws Exception
     */
    public function initConnection()
    {
        $config = Yaf\Registry::get('config');

        $connection_name = $this->getConnectionName();

        if (null != $connection_name) {
            $this->_connection_name = $connection_name;
        }

        if (!isset($config['db'][$this->_connection_name])) {
            throw new \Exception('Not find db ' . $this->_connection_name .' config in config file');
        }

        if (isset(self::$_connection[$this->_connection_name])) {
            return true;
        }

        $manager = new Manager();

        $this->_attributes =  $config['db'][$this->_connection_name]->toArray() + $this->_attributes;
        $manager->addConnection($this->_attributes, $this->_connection_name);
//        $manager->setEventDispatcher(new Dispatcher(new Container));

        // Set the cache manager instance used by connections... (optional)
//        $manager->setCacheManager();

        // Make this Capsule instance available globally via static methods... (optional)
//        $manager->setAsGlobal();

        // Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
        $manager->bootEloquent();

        self::$_connection[$this->_connection_name] = $manager;

        return true;
    }
}