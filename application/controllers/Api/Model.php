<?php
/**
 * @name IndexController
 * @author xiaohuzi
 * @desc 默认控制器
 * @see http://php.net/manual/zh/class.yaf-controller-abstract.php
 */
class Api_ModelController extends Yaf\Controller_Abstract
{
    
    public function indexAction()
    {
        $model = new UserModel();
    //    $res = $model->find(2);

   //     $users = UserModel::where('id', '>', 1)->get();

        $foo = new FooModel();
        $user = $foo->find(1);
        var_dump($user->toArray());

    }

    public function ExceptionAction()
    {
        throw new Exception("Error Processing Request", 1);
        
    }
    
}
