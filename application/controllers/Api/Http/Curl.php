<?php
use \Curl\Curl;
use \Monolog\Logger;
use \Monolog\Handler\StreamHandler;


/**
 * @name IndexController
 * @author xiaohuzi
 * @desc 默认控制器
 * @see http://php.net/manual/zh/class.yaf-controller-abstract.php
 */
class Api_Http_CurlController extends Yaf\Controller_Abstract
{

    public function indexAction()
    {
        $curl = new Curl();

        $curl->get('http://www.baidu.com');
        var_dump($curl->response);

    }

    public function ExceptionAction()
    {
        throw new Exception("Error Processing Request", 1);

    }

}
