<?php
/**
 * @name ServiceController
 * @author xiaohuzi
 * @desc 服务测试
 * @see http://php.net/manual/zh/class.yaf-controller-abstract.php
 */
class ServiceController extends Yaf\Controller_Abstract
{

    /**
     * map 路由模式只有indexAction可以访问
     */
    public function indexAction()
    {
        \App\Service\Test::foo();
    }

}
