<?php

/**
 * Http工具类,对\Curl\Curl
 */

use \Curl\Curl;

class Http extends Curl
{
    protected static $_instance = null;
    /**
     * 安全通道开关
     *
     * @var bool
     */
    protected $safeMode = false;

    protected $is_encrypt = false;

    /**
     * 把请求发往事件,而不真正的请求外部接口
     *
     * @var bool
     */
    protected $pretending = false;

    /**
     * 假请求事件
     * @var null
     */
    protected $pretendingCallback = null;

    /**
     * 安全通道参数
     *
     * @var array
     */
    protected $ssl;

    /**
     * 端口号,用于显示
     * @var int
     */
    protected $_port = 80;

    /**
     * 请求参数如果是数组,是否用json格式
     * @var bool
     */
    protected $jsonRequest = true;

    /**
     * 关闭应答内容日志
     * @var bool
     */
    protected $response_log = true;

    /**
     * POST 请求接口的原数据
     *
     * @var array
     */
    protected $lastRawRequest = [];

    /**
     * 最后一次调用接口返回的值(已解密)
     *
     * @var array|mixed
     */
    protected $lastRawResponse;

    /**
     * 当前允许的调用参数
     * @var array
     */
    protected $methods = ['get', 'post', 'put', 'patch', 'delete'];

    protected $errorFunc = null;

    public function __construct($base_url, $ssl)
    {
        parent::__construct($base_url);

        if (is_array($ssl)) {

            $this->setSSl($ssl);
        }

        //重置组件中的json解码器
        $this->setJsonDecoder(function ($response) {
            $data = json_decode($response, true);
            if (json_last_error() == JSON_ERROR_NONE) {
                $response = $data;
            }

            return $response;
        });

        $this->completeFunction = function () {
            Log::info(sprintf("[ServerApi] URL: %s Port: %s |HTTP状态码: %s|HTTP状态信息: %s", $this->url, $this->_port, $this->httpStatusCode, $this->httpError));
            if (!empty($this->lastRawRequest)) {
                Log::info('[ServerApi] Param ' . json_encode($this->lastRawRequest, JSON_UNESCAPED_UNICODE));
            }
        };

        $this->errorFunc = function(){
            $curl_msg = sprintf("[ServerApi] URL: %s Port: %s |错误码: %s|错误信息: %s|HTTP状态码: %s|HTTP状态信息: %s",
                $this->url, $this->_port, $this->curlErrorCode, $this->errorMessage, $this->httpStatusCode, $this->httpError);

            throw new TransferErrorException($curl_msg, $this->httpStatusCode, $this->curlErrorCode);
        };

        $this->pretendingCallback = function($method, $url, $param){
            return [];
        };
    }

    public static function instance($base_url = '', $ssl = [])
    {
        if (self::$_instance == null) {
            self::$_instance = new self($base_url, $ssl);
        }

        return self::$_instance;
    }

    public function encrypt()
    {
        $this->is_encrypt = true;
        return $this;
    }

    /**
     * POST
     *
     * @param       $url
     * @param array $data
     * @param bool  $follow_303_with_post
     *
     * @return string
     */
    public function post($url, $data = [], $follow_303_with_post = false)
    {
        $this->lastRawRequest = $data;
        return $this->safeExec(__FUNCTION__, $url, $data, $follow_303_with_post);
    }


    /**
     * GET
     *
     * @param  $url
     * @param  $data
     *
     * @return string
     */
    public function get($url, $data = [])
    {
        return $this->safeExec(__FUNCTION__, $url, $data);
    }

    /**
     * PUT
     *
     * @param  $url
     * @param  $data
     *
     * @return string
     */
    public function put($url, $data = [])
    {
        return $this->safeExec(__FUNCTION__, $url, $data);
    }


    /**
     * 获取最后操作的接口原应答信息
     *
     * @return array
     */
    public function getLastRawResponse()
    {
        return $this->lastRawResponse;
    }


    protected function buildURL($url, $data = [])
    {
        return $url . (empty($data) ? '' : '?' . http_build_query($data));
    }

    /**
     * 设置安全通道参数并开启安全通道开关
     *
     * @param $ssl
     *
     * @throws \Exception
     */
    public function setSSl($ssl, $open = false)
    {
        $this->ssl = $ssl;

        $this->safeMode($open);

        return $this;
    }

    /**
     * Set Port
     *
     * @access public
     * @param  $port
     */
    public function setPort($port)
    {
        $this->_port = $port;
        $this->setOpt(CURLOPT_PORT, intval($port));
        return $this;
    }

    /**
     * 安全通道开关
     * @param bool $open
     *
     * @return $this
     * @throws \Exception
     */
    public function safeMode($open = false)
    {
        if ($open && $this->ssl == null) {
            throw new \Exception('没有设置ssl参数,请配置ssl参数或者设置服务器 $is_safe = false', 500);
        }

        $this->safeMode = $open;

        return $this;
    }

    /**
     * 请求内容模式
     *
     * @param bool $has
     *
     * @return $this
     */
    public function jsonRequest($has = true)
    {
        $this->jsonRequest = $has;

        return $this;
    }

    /**
     * 设置是否向日志中打印返回内容
     * @param bool $open
     *
     * @return static
     */
    public function responseLog($open = false)
    {
        $this->response_log = $open;

        return $this;
    }

    /**
     * 假请求开关
     * @param bool $open
     *
     * @return static
     */
    public function pretending($open)
    {
        $this->pretending = $open;

        if ($this->pretending == true && !is_callable($this->pretendingCallback)) {
            throw new \Exception('[ServerApi] error |pretendingCallback没有设置');
        }

        return $this;
    }

    /**
     * 设置假发请求事件的callback
     * @param callable $callback
     */
    public function setPretendingCallback(callable $callback, $open = false)
    {
        $this->pretendingCallback = $callback;

        $this->pretending = $open;
    }

    /**
     * 执行假请求的callback
     *
     * @param $method
     * @param $url
     * @param $param
     */
    protected function firePretendingCallback($method, $url, $param)
    {
        $response = call_user_func($this->pretendingCallback, $method, $url, $param);

        $this->httpStatusCode = 200;

        $this->httpError = '模拟接口返回';

        call_user_func($this->completeFunction);

        return $response;
    }


    /**
     * 接口加密器
     *
     * @param $data
     *
     * @return mixed
     */
    protected function _encrypt($data)
    {
        return $data;
    }

    /**
     *
     * 接口解密器
     * @param $data
     *
     * @return array|mixed
     * @throws Exceptions;
     */
    protected function _decrypt($data)
    {
        if ($this->safeMode) {
            $this->lastRawResponse = $data;
        } else {
            $this->lastRawResponse = $data;
        }

        return $this->lastRawResponse;
    }

    /**
     * 执行HTTP请求包装器
     *
     * @param      $method
     * @param      $uri
     * @param      $data
     * @param bool $follow_303_with_post
     *
     * @return array|mixed
     * @throws Exception
     */
    protected function safeExec($method, $uri, $data, $follow_303_with_post = false)
    {
        if ($this->is_encrypt) {
            $data = $this->_encrypt($data);
        }

        if (!in_array($method, $this->methods)) {
            throw new Exception($this->methods, 'ServerApi 不允许使用 ' . $method);
        }

        //Java服务器不支持json数据调用
        if (is_array($data) && $this->jsonRequest) {
            $this->setHeader('Content-Type', 'application/json');
        }

        $start_time = microtime(true);

        if ($this->pretending == true) {
            $this->setURL($uri, $data);
            $response = $this->firePretendingCallback($method, $this->url, $data);
        } else {
            $response = parent::$method($uri, $data, $follow_303_with_post);
        }
        $end_time = microtime(true);

        if ($this->error) {
            $this->call($this->errorFunc);
        }

        if ($this->is_encrypt) {
            $response = $this->_decrypt($response);
        }

        $resp_log = $this->response_log ? json_encode($response, JSON_UNESCAPED_UNICODE) : 'ignore';
        Log::info('[ServerApi] Response |耗时:' . round($end_time - $start_time, 2) . 's|' . $resp_log);

        return $response;
    }
}