<?php
/**
 * @name LogController
 * @author xiaohuzi
 * @desc 默认控制器
 * @see http://php.net/manual/zh/class.yaf-controller-abstract.php
 */
class LogController extends Yaf\Controller_Abstract
{

    /**
     * map 路由模式只有indexAction可以访问
     */
    public function indexAction()
    {
//        throw  new Exception('test');
        Log::info('this is a log');
        Log::warning('file', ['php' => 'go'], 'mylog');
        Log::debug(['php' => 'ok']);

       // $user = new UserModel();
       // Log::info($user);
    }

}
