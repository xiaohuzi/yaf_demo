<?php
/**
 * @name User
 * @author xiaohuzi
 * @desc Model类
 * @see http://www.php.net/manual/en/class.yaf-controller-abstract.php
 */
class UserModel extends BaseModel
{
    protected $connection = 'mysql';
    protected $table = 'users';
}