<?php
/**
 * @name Bootstrap
 * @author xiaohuzi
 * @desc 所有在Bootstrap类中, 以_init开头的方法, 都会被Yaf调用,
 * @see http://www.php.net/manual/en/class.yaf-bootstrap-abstract.php
 * 这些方法, 都接受一个参数:Yaf\Dispatcher $dispatcher
 * 调用的次序, 和申明的次序相同
 */
class Bootstrap extends Yaf\Bootstrap_Abstract
{
    private $_config;
    
    public function _initConfig($dispatcher)
    {
        //取配置
        $this->_config = Yaf\Application::app()->getConfig();
        Yaf\Registry::set('config', $this->_config);
    }
    
    /*
     * 初始化页面展示数据
     */
    public function _initView()
    {
        /*
        $show['js_config']['type'] = $this->_config->js_config->type;
        $show['js_config']['path'] = $this->_config->js_config->path;
        Yaf\Registry::set('show', $show);
         * 
         */
        //关闭自动加载
        Yaf\Dispatcher::getInstance()->autoRender(FALSE);
    }

    /**
     * 注册map路由
     * @param Yaf\Dispatcher $dispatcher
     */
    public function _initRoute(Yaf\Dispatcher $dispatcher)
    {
        $router = $dispatcher->getRouter();

        $routes = new Yaf\Route\Map(true);
        $router->addRoute('router_map', $routes);
    }


    public function _initLoader()
    {
       /**
        *  Yaf\Loader::import是根据路径引入
        *  Yaf\Loader::getInstance()->autoload 在library目录下的的路径
        *  没有namespace的DB在library根目录下默认引入
        */
       Yaf\Loader::import(APPLICATION_PATH . "/vendor/autoload.php");
   }
}
